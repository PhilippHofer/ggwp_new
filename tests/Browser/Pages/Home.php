<?php

namespace Tests\Browser\Pages;

class Home extends Page
{
    /**
     * sudo gitlab-runner-linux-amd64 exec docker test:app
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/home';
    }

    /**
     * Click on the log out link.
     *
     * @param  \Laravel\Dusk\Browser $browser
     * @return void
     */
    public function clickLogout($browser)
    {
        $browser->clickLink('LOGOUT')
            ->pause(300);
    }
}
